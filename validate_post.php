<?php

    is_form_filled();

    function is_form_filled() {
        $required_fields = ['name', 'email', 'message_text'];
        foreach ($required_fields as $key) {
            if(empty($_POST[$key])) {
                $response = array ('status' => 'fail',
                                'data' => ['error_msg' => 'missing value for ' . $key,
                                'error_field' => $key]);
                header('Content-Type: application/json', false, 400);
                die(json_encode ($response));
            }
        }

    }

    require("mysql_connection.php");
    require("mail.php");
//  require_once "recaptchalib.php";
    //  
    // if(validate_reCaptcha())
    // {
    //  //
        if (validate_name($_POST['name']) && (validate_email($_POST['email']))) {
            connection_bd($_POST['name'],$_POST['email'],$_POST['message_text']);
            email_newsletter($_POST['name'], $_POST['email'], $_POST['message_text']);
            $response = array ('status' => 'success',
                                'data' => 'Your data was saved and an email was sent succesfully!');
            header('Content-Type: application/json', false, 200);
            die(json_encode ($response));
        }
    // }
    // else
    // {
    //  echo "fail_captcha";
    // }
    //
    function validate_name($name) {
        if(ctype_alpha(str_replace(' ', '', $name))) {
            return true;
        } else { 
            $response = array ('status' => 'fail',
                                'data' => ['error_msg' => 'please insert a valid name.',
                                'error_field' => 'name']);
            header('Content-Type: application/json', false, 400);
            die(json_encode ($response));
        }
    }   
    //
    function validate_email($email) {

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $response = array ('status' => 'fail',
                                'data' => ['error_msg' => 'please insert a valid email.',
                                'error_field' => 'email']);
            header('Content-Type: application/json', false, 400);
            die(json_encode ($response));
        }
    }
    //
    function validate_reCaptcha() {
        $secret = "6LcLkQ0UAAAAAHJXgNBkOeR-DzaGqE3qHN0Kzw-i";
        $response = null;
        $reCaptcha = new ReCaptcha($secret);    
        //
        if ($_POST["g-recaptcha-response"]) {
            $response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]);
        }
        if ($response != null && $response->success) {
            return true;
        } else {
            return false;
        }
    }
 ?>