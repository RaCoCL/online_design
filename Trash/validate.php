<script src="jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="jquery.form.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function() {
      $("#validate").hide();
      $("#formid").validate({
        rules: {
            name: { required: true, minlength: 2},            
            email: { required:true, email: true},
        },
        messages: {
            name: "Please, enter your name",            
            email : "you must enter a valid email.",            
        },
        submitHandler: function(form){
            var dataString = 'name='+$('#name').val();
            $.ajax({
                type: "POST",
                url:"validate.php",
                data: dataString,
                success: function(data){
                    $("#validate").html(data);
                    $("#validate").show();
                    $("#formid").hide();
                }
            });
        }
    });
});
</script>