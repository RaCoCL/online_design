<?php
function email_newsletter($name,$email,$message) {
	$to      = $email;
	$title    = 'new user in: ' . $_SERVER['HTTP_HOST'];
	$message   = 'new user: name.' .$name. "\r\n".'email: '.$email. "\r\n". 'message: ' .$message;
	$header = 'From: jlizanam@gmail.com' . "\r\n" .
	    'Reply-To: jlizanam@gmail.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

	if(!mail($to, $title, $message, $header)) {
        $response = array ('status' => 'error',
                            'data' => ['error_msg' => 'There was an error sending your email.']);
        header('Content-Type: application/json', false, 500);
        die(json_encode ($response));
	}
}
?>