$(document).ready(function () {
  $('#submit').on('click', function () {
    // Initiate Variables With Form Content
    var name = $('#name').val()
    var email = $('#email').val()
    var message = $('#message_text').val()
    // var recaptcha = $("#g-recaptcha-response").val()

    $.ajax({
      type: 'POST',
      url: 'validate_post.php',
      dataType: 'json',
      data: $('#form_test').serialize(),
      success: function (data, statusDetail, xhr) {
        // if everything is good
        if (xhr.status === 200) {
          formSuccess()
        }
      }
    }).fail(function (xhr, textStatus) {
      var json = $.parseJSON(xhr.responseText)
      show_error_field(json.data.error_field, json.data.error_msg)
    })
  })

  // We are going to show errors according to the JSON response we got from validate_post.php
  function show_error_field(field, message) {
    $('.alert').remove()
    $('<div id="alert" class="alert alert-warning">' + message + '</div>').insertAfter('#' + field)
  }

  function formSuccess () {
    $('.form_test form').hide().fadeOut()
    $('.form-newsletter').replaceWith('<div class="alert alert-success">thanks for subscribe in this test!</div>')
  }
  function formError () {
    $('.form_test form').hide().fadeOut()
    $('.form-newsletter').replaceWith('<div class="alert alert-danger">We have a problem with this web site. please try again in oher moment.</div>')
  }

})

